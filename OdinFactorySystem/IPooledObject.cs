﻿// Odin Factory System
// copyright Mikołaj Bystrzyński

// ReSharper disable All
namespace OdinFactorySystem
{
	public interface IPooledObject : IUnityObject
	{
		#region PublicFields

		bool Free { get; set; }

		#endregion

		#region PublicMethods

		void Begin(IPooledObjectProperties properties);
		void Execute();
		void End();

		#endregion
	}
}
