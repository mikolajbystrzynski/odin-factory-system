﻿using UnityEngine;

// Odin Factory System
// copyright Mikołaj Bystrzyński

// ReSharper disable All
namespace OdinFactorySystem
{
	public interface IUnityObject
	{
		#region PublicFields

		// ReSharper disable once InconsistentNaming
		Behaviour behaviour { get; }

		// ReSharper disable once InconsistentNaming
		GameObject gameObject { get; }

		// ReSharper disable once InconsistentNaming
		Transform transform { get; }

		#endregion
	}
}
