﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

// Odin Factory System
// copyright Mikołaj Bystrzyński

// ReSharper disable All
namespace OdinFactorySystem
{
	public static class MarshallFactory
	{
		#region PrivateFields

		private static Dictionary<int, List<IPooledObject>> pool;

		#endregion

		#region PublicMethods

		public static IPooledObject GetObject(IPooledObject pooledObject)
		{
			return pooledObject.GetPooledObject();
		}

		public static IPooledObject GetPooledObject(this IPooledObject pooledObject)
		{
			ConditionalInit();

			return Internal_GetPooledObject(pooledObject);
		}

		#endregion

		#region PrivateMethods

		private static IPooledObject Internal_GetPooledObject(IPooledObject pooledObject)
		{
			var pooledType = pooledObject.GetHashCode();
			List<IPooledObject> poolRef;

			try
			{
				poolRef = pool[pooledType];
			}
			catch
			{
				poolRef = new List<IPooledObject>();
				pool.Add(pooledType, poolRef);
			}

			poolRef.RemoveAll(item => item == null);

			IPooledObject retPooledObject = null;

			foreach (var pooledProj in poolRef)
			{
				if (pooledProj.Free)
				{
					retPooledObject = pooledProj;
					pool[pooledType] = poolRef;
					return retPooledObject;
				}
			}

			try
			{
				var go = Object.Instantiate(pooledObject.behaviour, Vector3.zero, Quaternion.identity);

				if (go is IPooledObject)
				{
					retPooledObject = go as IPooledObject;
					retPooledObject.Free = true;
					poolRef.Add(retPooledObject);
				}
				else
				{
					Debug.LogError(pooledObject.behaviour + " is not of type IProjectile");
					pool[pooledType] = poolRef;
					return null;
				}
			}
			catch
			{
				Debug.LogError($"Incorrect factory setup for {pooledObject.GetType()}");
				pool[pooledType] = poolRef;
				return null;
			}

			pool[pooledType] = poolRef;
			return retPooledObject;
		}

		private static void ConditionalInit()
		{
			if (pool == null)
			{
				pool = new Dictionary<int, List<IPooledObject>>();
			}
		}

		#endregion
	}
}
