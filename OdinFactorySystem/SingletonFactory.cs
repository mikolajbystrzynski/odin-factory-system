﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

// Odin Factory System
// copyright Mikołaj Bystrzyński

// ReSharper disable All
namespace OdinFactorySystem
{
	public abstract class SingletonFactory<TEnum, TSingleton> : SerializedScriptableObject, IFactory<TEnum>
		where TSingleton : SingletonFactory<TEnum, TSingleton>
	{
		#region Singleton

		public static TSingleton Instance
		{
			get
			{
				if (instance == null)
				{
					instance = Resources.Load<TSingleton>(typeof(TSingleton).Name);

					if (instance != null)
					{
						InitPools();
					}
				}

				return instance;
			}
		}

		private static TSingleton instance;

		#endregion

		#region PublicFields

		public Dictionary<TEnum, List<IPooledObject>> Pools { get; private set; }
		public Dictionary<TEnum, IPooledObject> Prefabs => prefabs;

		#endregion

		#region SerializeFields

		[OdinSerialize] private Dictionary<TEnum, IPooledObject> prefabs;

		#endregion

		#region UnityMethods

		private void OnDestroy()
		{
			CleanupPools();
		}

		#endregion

		#region PublicMethods

		public static IPooledObject GetObject(TEnum type)
		{
			return Instance.Get(type);
		}

		public IPooledObject Get(TEnum type)
		{
			Pools[type].RemoveAll(item => item == null);

			IPooledObject projectile = null;

			foreach (var pooledProj in Pools[type])
			{
				if (pooledProj.Free)
				{
					projectile = pooledProj;
					return projectile;
				}
			}

			try
			{
				var go = Instantiate(prefabs[type].behaviour, Vector3.zero, Quaternion.identity);

				if (go is IPooledObject)
				{
					projectile = go as IPooledObject;
					projectile.Free = true;
					Pools[type].Add(projectile);
				}
				else
				{
					Debug.LogError(prefabs[type].behaviour + " is not of type IProjectile");
					return null;
				}
			}
			catch (Exception ex)
			{
				Debug.LogError($"Incorrect factory setup for {type}");

				throw ex;
			}

			return projectile;
		}

		public void CleanupPools()
		{
			Debug.Log("Cleaning up pools");

			foreach (var pool in Pools)
			{
				foreach (var poolElement in pool.Value)
				{
					if (poolElement != null && poolElement.gameObject != null)
					{
						DestroyImmediate(poolElement.gameObject);
					}
				}

				pool.Value.Clear();
			}
		}

		#endregion

		#region PrivateMethods

		private static void InitPools()
		{
			instance.Pools = new Dictionary<TEnum, List<IPooledObject>>();

			foreach (var prefab in instance.prefabs)
			{
				instance.Pools.Add(prefab.Key, new List<IPooledObject>());
			}
		}

		#endregion
	}
}
