﻿using System.Collections.Generic;

// Odin Factory System
// copyright Mikołaj Bystrzyński

// ReSharper disable All
namespace OdinFactorySystem
{
	public interface IFactory<TEnum>
	{
		#region PublicFields

		Dictionary<TEnum, IPooledObject> Prefabs { get; }
		Dictionary<TEnum, List<IPooledObject>> Pools { get; }

		#endregion

		#region PublicMethods

		IPooledObject Get(TEnum type);

		#endregion
	}
}
