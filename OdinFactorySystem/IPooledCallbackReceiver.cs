﻿// Odin Factory System
// copyright Mikołaj Bystrzyński

// ReSharper disable All
namespace OdinFactorySystem
{
	public interface IPooledCallbackReceiver
	{
		#region PublicMethods

		void OnBegin(IPooledObject pooledObject);
		void OnEnd();

		#endregion
	}
}
